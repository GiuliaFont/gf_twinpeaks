"""
URL configuration for gf_twinpeaks project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
"""
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from gf_twinpeaks.views import Login, home

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", home, name="home"),
    path("login/", Login.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("", include("twinpeaks_post.urls")),
    path("", include("viewpoints.urls"))
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

