from django.apps import AppConfig


class TwinpeaksPostConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "twinpeaks_post"
