from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'title_tag', 'date', 'author', 'body')

        widgets = {
            'title': forms.TextInput
                     (
                     attrs=
                     {
                       'class': 'form-control',
                       'placeholder': 'This is a placeholder',
                     }
                     ),
            'title_tag': forms.TextInput
                     (
                     attrs=
                     {
                       'class': 'form-control',
                       'placeholder': 'title tag placeholder'
                     }
                     ),
            'date': forms.SelectDateWidget
                    (
                    attrs=
                    {
                      'class': 'form-control',
                       
                    }
                    ),
            'author': forms.Select
                      (
                      attrs=
                      {
                        'class': 'form-control',
                        'placeholder': 'select author'
                      }
                      ),
            'body': forms.Textarea
                    (
                    attrs=
                    {
                    'class': 'form-control',
                    'placeholder' : 'enter text here',
                    }
                    ),

        }