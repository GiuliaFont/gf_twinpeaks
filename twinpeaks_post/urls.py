from django.urls import path
from twinpeaks_post.views import PostView, ArticleDetailView, AddPostView, EditPostView, DeletePostView

urlpatterns = [
    path("articlelist/", PostView.as_view(), name="article-list"),
    path("article/<int:pk>", ArticleDetailView.as_view(), name="article-detail"),
    path("add_post/", AddPostView.as_view(), name="add_post"),
    path("article/edit/<int:pk>", EditPostView.as_view(), name="edit-post"),
    path("article/<int:pk>/", DeletePostView.as_view(), name="delete-post"),
] 