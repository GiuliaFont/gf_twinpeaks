from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from twinpeaks_post.models import Post
from .forms import PostForm
from django.urls import reverse_lazy
# Create your views here.


class PostView(ListView):
    model = Post
    template_name = "article_list.html"


class ArticleDetailView(DetailView):
    model = Post
    template_name = "article_details.html"


class AddPostView(CreateView):
    model = Post
    form_class = PostForm
    template_name = "add_post.html"



class EditPostView(UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'update_post.html'



class DeletePostView(DeleteView):
    model = Post
    template_name = "delete_post.html"

    success_url = reverse_lazy('article-list')