from django import forms
from .models import Business, Places

class BusinessForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = ('name', 'owner', 'description')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'add a business name'}),
            'owner': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'add a business owner name'}),
            'description': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'add a business description'}),
        }

class PlaceForm(forms.ModelForm):
    class Meta:
        model = Places
        fields = ('image','name', 'businesstype', 'description')
        
        widgets = {
            'image': forms.FileInput(attrs={'class': 'form-control', 'placeholder': 'insert image file'}),
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'add a name'}),
            'businesstype': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'add a business type'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'add a description'})
        }