from django.db import models


class Business(models.Model):
    name = models.CharField(max_length=50)
    owner = models.CharField(max_length=50)
    description = models.CharField(max_length=600)

    def __str__(self):
        return self.name


class Places(models.Model):
    image = models.ImageField()
    name = models.CharField(max_length=70)
    businesstype = models.ForeignKey(Business, on_delete=models.CASCADE)
    description = models.TextField(max_length=600)

    def __str__(self):
        return self.name