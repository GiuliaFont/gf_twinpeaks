from django.contrib import admin

from .models import Business, Places

# Register your models here.
admin.site.register(Business)
admin.site.register(Places)
