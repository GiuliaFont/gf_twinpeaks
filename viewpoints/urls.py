from django.urls import path
from . import views
from .views import BusinessCreateView, BusinessDetailView, BusinessListView, BusinessUpdateView, BusinessDeleteView, PlacesCreateView, PlacesDeleteView, PlacesDetailView, PlacesListView, PlacesUpdateView

urlpatterns = [
    path('businesslist/', BusinessListView.as_view(), name="business-list"),
    path('business/<int:pk>/', BusinessDetailView.as_view(), name="business-detail"),
    path('add_business/', BusinessCreateView.as_view(), name="add-business"),
    path('business/edit/<int:pk>', BusinessUpdateView.as_view(), name="edit-business"),
    path('business/<int:pk>/', BusinessDeleteView.as_view(), name="delete-business"),
    path('placeslist/', PlacesListView.as_view(), name="place-list"),
    path('place/<int:pk>', PlacesDetailView.as_view(), name="place-detail"),
    path('add_place/', PlacesCreateView.as_view(), name="add-place"),
    path('place/edit/<int:pk>', PlacesUpdateView.as_view(), name="edit-place"),
    path('place/<int:pk>/', PlacesDeleteView.as_view(), name="delete-place"),

]