from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from viewpoints.models import Business, Places
from .forms import BusinessForm, PlaceForm
from django.urls import reverse_lazy


class BusinessListView(ListView):
    model = Business
    template_name = "business_list.html"


class BusinessDetailView(DetailView):
    model = Business
    template_name = "business_details.html"


class BusinessCreateView(CreateView):
    model = Business
    form_class = BusinessForm
    template_name = "add_business.html"


class BusinessUpdateView(UpdateView):
    model = Business
    form_class = BusinessForm
    template_name = "update_business.html"


class BusinessDeleteView(DeleteView):
    model = Business
    template_name = "delete_business.html"

    success_url = reverse_lazy('business-list')


class PlacesListView(ListView):
    model = Places
    template_name = "place_list.html"


class PlacesDetailView(DetailView):
    model = Places
    template_name = "place_details.html"


class PlacesCreateView(CreateView):
    model = Places
    form_class = PlaceForm
    template_name = "add_place.html"


class PlacesUpdateView(UpdateView):
    model = Places
    form_class = PlaceForm
    template_name = "update_place.html"


class PlacesDeleteView(DeleteView):
    model = Places
    template_name = "delete_place.html"

    success_url = reverse_lazy('place-list')


def place(request):
    place = Places.objects.all()
    return render(request, 'viewpoints/place_details.html', { 'place' : place })


def business(request):
    business = Business.objects.all()
    return render(request, 'viewpoints/business_details.html', {'business' : business })